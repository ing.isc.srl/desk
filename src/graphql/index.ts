import { split } from 'apollo-link';
import { ApolloClient } from 'apollo-client';
import { WebSocketLink } from 'apollo-link-ws';
import { getMainDefinition } from 'apollo-utilities';
import { createHttpLink } from 'apollo-link-http';
import { setContext } from 'apollo-link-context';
import { InMemoryCache } from 'apollo-cache-inmemory';

const httpLink = createHttpLink({
  // uri: 'https://apps-ceoz.herokuapp.com/graphql',
  uri: 'http://localhost:4000/graphql',
});

const wsLink = new WebSocketLink({
  // uri: 'wss://apps-ceoz.herokuapp.com/subscriptions',
  uri: 'ws://localhost:4000/subscriptions',
  options: {
    reconnect: true,
    connectionParams: {
      headers: {
        authorization: sessionStorage.getItem('token'),
      },
    },
  },
});

const link = split(
  ({ query }) => {
    const def = getMainDefinition(query);
    return def.kind === 'OperationDefinition' && def.operation === 'subscription';
  },
  wsLink,
  httpLink,
);

const authLink = setContext((_: any, { headers }) => {
  return {
    headers: {
      ...headers,
      authorization: sessionStorage.getItem('token'),
    },
  };
});

export const client = new ApolloClient({
  link: authLink.concat(link),
  cache: new InMemoryCache(),
});
